### Lesson 1
Why programming?
 - IQ
 - logic

What is Python?
 - General purpose, often get applied in scripting roles.

Why do people Use Python ?
 - software quality : readability, coherence
 - productivity 1/3 1/5 of C++ or Java code
 - portability 
 - good library support
 - component integration with C, C++, Java, .Net
 - Enjoyment

What are the downsides?
 - excution speed not as good as fully compiled languages like C/C++
 - some application needs to get closer to the 'Iron'
 
Who uses python ?
 - Google
 - Dropbox
 - Raspberry Pi
 - Google App Engine
 - NSA 

What can I do with Python?
 - website development 
 - gaming
 - robotics
 - common applications include:
	- shell tools, system programming
	- GUIs
	- Internet Scripting, Paese and generate XML and JSON
	- data base programming
	- database programming eg. interface with Mysql, PostgresSQL
 	- Data mining and Scrapy web crawling

How is Python developed and supported?
 - Open source
 - Maintained by development community
 - faster speed response to issues than commercial ones
 - changes follows a strict protcol 
 - cons: can be chaotic and anarchy (无政府状）

Python programs?
 - Python Interpreter: program that executes other programs, a layer of software logic between your code and machine
 - after installation of python you mainly get: 1.interpreter(written in C) 2.support library
 - In theory interpreter has two parts:compiler (automatic process), PVM runtime engine.
Different point of views on python programs.
 - programmer: text file contains python statement eg. ```print('hello world')``` ```print(2**4)```
 - python interpreter:
 
How does python get exdcuted?
 - ![Excution model](./images/excution_model.png)
 - source code -> bytecode -> PVM (similar to JVM), Simplified CPU excution environment, not the same as emunate whole OS.
 - PVM the runtime engine of Python

 ```
 def hello()
    print("Hello, World!")
 ```

 ```
            0 LOAD_GLOBAL              0 (print)
            2 LOAD_CONST               1 ('Hello, World!')
            4 CALL_FUNCTION            1
 ```
 - https://opensource.com/article/18/4/introduction-python-bytecode
 - process GUI, File might run at C speed because it's dispatched compiled C code from intepreter
 - Numerical programming, animation, medical devices (eg. Numpy)
 
Interative Promt
 - experiment with python syntax on the fly
 - echo result 
 - it's a sandbox, so you won't break anything

Shell
 - shell: python3 morse.py icdipython > output.txt

Unix script
 - hashbang #!/usr/local/bin/python3 
 - 1.create file: icdi 2.give permission: chmod +x icdi 3. excute: ./icdi
 

IDE
 - Pycharm
 - VS code
Related links:
 - https://www.quora.com/Is-C-platform-dependent
 - https://qr.ae/TibpLt
 - https://en.wikipedia.org/wiki/Interpreter_(computing)
 - https://nedbatchelder.com/blog/201803/is_python_interpreted_or_compiled_yes.html
 - https://softwareengineering.stackexchange.com/questions/251250/why-do-executables-depend-on-the-os-but-not-on-the-cpu 
 - https://stackoverflow.com/questions/11810484/is-c-platform-dependent
 - https://stackoverflow.com/questions/861422/is-the-java-virtual-machine-really-a-virtual-machine-in-the-same-sense-as-my-vmw
 - https://zhuanlan.zhihu.com/p/72356928
